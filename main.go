package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

func main() {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	serverChan := make(chan chan string, 4)
	go uptimeServer(serverChan)

	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		ws, _ := upgrader.Upgrade(w, r, nil)
		client := make(chan string, 1)
		serverChan <- client

		for {
			select {
			case text, _ := <-client:
				writer, _ := ws.NextWriter(websocket.TextMessage)
				writer.Write([]byte(text))
				writer.Close()
			}
		}
	})

	http.ListenAndServe(":8880", nil)

}

func server(serverChan chan chan string) {
	var clients []chan string
	for {
		select {
		case client, _ := <-serverChan:
			clients = append(clients, client)
			for _, c := range clients {
				c <- fmt.Sprintf("%d client(s) connected.", len(clients))
			}
		}
	}
}

func client(clientName string, clientChan chan string) {
	for {
		text, _ := <-clientChan
		fmt.Printf("%s: %s\n", clientName, text)
	}
}

func uptimeServer(serverChan chan chan string) {
	var clients []chan string
	uptimeChan := make(chan int, 1)
	go func(target chan int) {
		i := 0
		for {
			time.Sleep(time.Second)
			i++
			target <- i
		}
	}(uptimeChan)
	for {
		select {
		case client, _ := <-serverChan:
			clients = append(clients, client)

		case uptime, _ := <-uptimeChan:
			for _, c := range clients {
				c <- fmt.Sprintf("%d seconds uptime", uptime)
			}
		}
	}
}
